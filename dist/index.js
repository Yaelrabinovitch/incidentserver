"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
require("./mongo");
const routes_1 = __importDefault(require("./routes"));
const config_1 = require("./config");
const app = express_1.default();
app.use(express_1.default.json());
app.use(body_parser_1.default.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    next();
});
// Register the route of the app
routes_1.default(app);
// start the express server
app.listen(config_1.PORT, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${config_1.PORT}`);
});
//# sourceMappingURL=index.js.map