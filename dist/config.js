"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PORT = exports.MONGO_CONNECTION_STRING = void 0;
const MONGO_CONNECTION_STRING = 'mongodb://localhost:27017/incident';
exports.MONGO_CONNECTION_STRING = MONGO_CONNECTION_STRING;
const PORT = 3000;
exports.PORT = PORT;
//# sourceMappingURL=config.js.map