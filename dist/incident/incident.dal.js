"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.update = exports.getAll = exports.getById = void 0;
const incident_model_1 = __importDefault(require("./incident.model"));
function getById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield incident_model_1.default.findById(id);
    });
}
exports.getById = getById;
function getAll() {
    return __awaiter(this, void 0, void 0, function* () {
        return yield incident_model_1.default.find();
    });
}
exports.getAll = getAll;
function update(id, prop) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield incident_model_1.default.update({ _id: id }, { $set: prop });
    });
}
exports.update = update;
//# sourceMappingURL=incident.dal.js.map