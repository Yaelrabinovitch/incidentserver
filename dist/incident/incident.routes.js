"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = exports.route = void 0;
const express_1 = __importDefault(require("express"));
const incident_dal_1 = require("./incident.dal");
const event_dal_1 = require("../event/event.dal");
const router = express_1.default.Router();
exports.router = router;
const route = 'incident';
exports.route = route;
router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    if (!id) {
        res.status(400);
    }
    else {
        const incident = yield incident_dal_1.getById(id);
        res.json(incident);
    }
}));
router.get('/events/:IncidentId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const IncidentId = req.params.IncidentId;
    if (!IncidentId) {
        res.status(400);
    }
    else {
        const events = yield event_dal_1.getByIncidentId(IncidentId);
        res.json(events);
    }
}));
router.post('/update/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const incidentId = req.params.id;
    if (!incidentId) {
        res.status(400);
    }
    else {
        const { propName, oldVal, newVal } = req.body;
        yield incident_dal_1.update(incidentId, { [propName]: newVal });
        yield event_dal_1.addEvent({
            incident: incidentId,
            oldVal,
            newVal,
            eventType: propName
        });
        res.json({ incidentId });
    }
}));
router.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const incidents = yield incident_dal_1.getAll();
    res.json(incidents);
}));
//# sourceMappingURL=incident.routes.js.map