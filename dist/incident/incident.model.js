"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Country = exports.Status = exports.Priority = void 0;
const mongoose_1 = require("mongoose");
exports.Priority = Object.freeze({
    Low: "LOW",
    Medium: "MEDIUM"
});
exports.Status = Object.freeze({
    Open: "OPEN",
    Close: "CLOSE"
});
exports.Country = Object.freeze({
    US: "US",
    Israel: "ISRAEL"
});
const schema = new mongoose_1.Schema({
    status: {
        type: String,
        enum: Object.values(exports.Status)
    },
    priority: {
        type: String,
        enum: Object.values(exports.Priority)
    },
    country: {
        type: String,
        enum: Object.values(exports.Country)
    }
});
exports.default = mongoose_1.model("Incident", schema);
//# sourceMappingURL=incident.model.js.map