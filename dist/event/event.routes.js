"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = exports.route = void 0;
const express_1 = __importDefault(require("express"));
const event_dal_1 = require("./event.dal");
const router = express_1.default.Router();
exports.router = router;
const route = 'event';
exports.route = route;
router.post('/addManual/:incidentId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const incidentId = req.params.incidentId;
    if (!incidentId) {
        res.status(400);
    }
    else {
        const { description, name } = req.body;
        const newEvent = yield event_dal_1.addEvent({
            incident: incidentId,
            eventType: "manual",
            description,
            name
        });
        res.send(newEvent);
    }
}));
//# sourceMappingURL=event.routes.js.map