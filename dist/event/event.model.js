"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const schema = new mongoose_1.Schema({
    newVal: String,
    oldVal: String,
    description: String,
    name: String,
    eventType: String,
    timestamp: Date,
    incident: { type: mongoose_1.Schema.Types.ObjectId, ref: "Incident" },
});
exports.default = mongoose_1.model("Event", schema);
//# sourceMappingURL=event.model.js.map