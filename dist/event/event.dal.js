"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addEvent = exports.getByIncidentId = void 0;
const event_model_1 = __importDefault(require("./event.model"));
const mongodb_1 = require("mongodb");
function getByIncidentId(id) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield event_model_1.default.find({ incident: new mongodb_1.ObjectID(id) });
    });
}
exports.getByIncidentId = getByIncidentId;
function addEvent(event) {
    return __awaiter(this, void 0, void 0, function* () {
        const newEvent = Object.assign(Object.assign({}, event), { incident: new mongodb_1.ObjectID(event.incident), timestamp: new Date() });
        return yield new event_model_1.default(newEvent).save();
    });
}
exports.addEvent = addEvent;
//# sourceMappingURL=event.dal.js.map