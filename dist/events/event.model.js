"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const schema = new mongoose_1.Schema({
    newVal: String,
    oldVal: String,
    description: String,
    title: String,
    eventType: String
});
exports.default = mongoose_1.model("events", schema);
//# sourceMappingURL=event.model.js.map