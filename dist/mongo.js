"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const config_1 = require("./config");
mongoose_1.default.connect(config_1.MONGO_CONNECTION_STRING, { useNewUrlParser: true })
    .catch(err => {
    // tslint:disable-next-line:no-console
    console.error(`Failed to connect to MongoDb\n ${err}`);
});
//# sourceMappingURL=mongo.js.map