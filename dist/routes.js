"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const incident_routes_1 = require("./incident/incident.routes");
const event_routes_1 = require("./event/event.routes");
exports.default = (app) => {
    app.use(`/api/${incident_routes_1.route}`, incident_routes_1.router);
    app.use(`/api/${event_routes_1.route}`, event_routes_1.router);
};
//# sourceMappingURL=routes.js.map