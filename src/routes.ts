import { route as incidentRoute, router as incidentRouter } from './incident/incident.routes';
import { route as eventRoute, router as eventRouter } from './event/event.routes';

export default (app: any) => {
    app.use(`/api/${incidentRoute}`, incidentRouter);
    app.use(`/api/${eventRoute}`, eventRouter);
  };
