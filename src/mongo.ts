import mongoose from 'mongoose';
import { MONGO_CONNECTION_STRING } from './config';

mongoose.connect(MONGO_CONNECTION_STRING, { useNewUrlParser: true })
    .catch(err => {
        // tslint:disable-next-line:no-console
        console.error(`Failed to connect to MongoDb\n ${err}`)
    });