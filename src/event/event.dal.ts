import Event from './event.model'
import { ObjectID } from 'mongodb';

export async function getByIncidentId(id: string) {
    return await Event.find({ incident: new ObjectID(id) });
}

export async function addEvent(event: {
    incident: string,
    newVal?: string,
    oldVal?: string,
    description?: string,
    name?: string,
    eventType: string
}) {
    const newEvent = { ...event, incident: new ObjectID(event.incident), timestamp: new Date() };
    return await new Event(newEvent).save();
}

