import { Schema, model } from "mongoose";


const schema = new Schema({
   newVal: String,
   oldVal: String,
   description: String,
   name: String,
   eventType: String,
   timestamp: Date,
   incident: { type: Schema.Types.ObjectId, ref: "Incident" },
});

export default model("Event", schema);
