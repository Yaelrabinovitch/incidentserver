
import express from "express";
import { addEvent, getByIncidentId } from './event.dal';

const router = express.Router();
const route = 'event';

router.post('/addManual/:incidentId', async (req, res) => {
    const incidentId = req.params.incidentId;
    if (!incidentId) {
        res.status(400);
    } else {
        const { description, name } = req.body;

         const newEvent = await addEvent({
            incident: incidentId,
            eventType: "manual",
            description,
            name
        });

        res.send(newEvent);
    }
});


export { route, router };