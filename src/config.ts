const MONGO_CONNECTION_STRING = 'mongodb://localhost:27017/incident';
const PORT = 3000;

export { MONGO_CONNECTION_STRING, PORT}