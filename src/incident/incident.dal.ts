import Incident from './incident.model'

export async function getById(id: string) {
  return await Incident.findById(id);
}

export async function getAll() {
  return await Incident.find();
}

export async function update(id: string, prop: any) {
  return await Incident.update({_id: id}, {$set: prop});
}


