
import express from "express";
import { getById, update, getAll } from './incident.dal';
import { addEvent, getByIncidentId, } from '../event/event.dal';
const router = express.Router();
const route = 'incident';

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(400);
    } else {
        const incident = await getById(id);
        res.json(incident);
    }
});

router.get('/events/:IncidentId', async (req, res) => {
    const IncidentId = req.params.IncidentId;
    if (!IncidentId) {
        res.status(400);
    } else {
        const events = await getByIncidentId(IncidentId);
        res.json(events);
    }
});

router.post('/update/:id', async (req, res) => {
    const incidentId = req.params.id;
    if (!incidentId) {
        res.status(400);
    } else {
        const { propName, oldVal, newVal } = req.body;
        await update(incidentId, { [propName]: newVal });
        await addEvent({
            incident: incidentId,
            oldVal,
            newVal,
            eventType: propName
        });

        res.json({incidentId});
    }
});

router.get('/', async (req, res) => {
    const incidents = await getAll();
    res.json(incidents);
});


export { route, router };