import { Schema, model } from "mongoose";

export const Priority = Object.freeze({
    Low: "LOW",
    Medium: "MEDIUM"
});

export const Status = Object.freeze({
    Open: "OPEN",
    Close: "CLOSE"
});

export const Country = Object.freeze({
    US: "US",
    Israel: "ISRAEL"
});


const schema = new Schema({
    status: {
        type: String,
        enum: Object.values(Status)
    },
    priority: {
        type: String,
        enum: Object.values(Priority)
    },
    country: {
        type: String,
        enum: Object.values(Country)
    }
});

export default model("Incident", schema);
